const express = require('express')
const router = express.Router()

/* GET home page*/
router.use(logger)

router.get('/', (req, res) => {
    res.render('index', {title: 'Index Page'})
})

function logger(req, res, next){
    console.log(req.originalUrl)
    next()
};

module.exports = router;