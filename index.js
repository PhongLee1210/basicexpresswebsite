const express = require('express')
const app = express()

app.set('view engine', 'ejs');

const homeRouter = require('./routes/home');
const userRouter = require('./routes/users');
const contactRouter = require('./routes/contact');
const aboutRouter = require('./routes/about');
// Access html tag
app.use(express.urlencoded({extended: true}))
// process json info
app.use(express.json())
// if enter /users add event at the end to callback in users.js
app.use('/', homeRouter);
app.use('/users', userRouter);
app.use('/contact', contactRouter);
app.use('/about', aboutRouter);


app.listen(3000);